// Problem:
// Given an array of integers greater than zero, find if it is possible to 
// split it in two (without reordering the elements), such that the sum of the 
// two resulting arrays is the same.

#include <vector>
#include <iostream>
#include <numeric>
#include <cassert>

bool CheckSplitInEqualSum(std::vector<int> & v) {
    int sum_left = 0;
    int sum_right = accumulate(v.begin(), v.end(), 0);

    if (sum_right % 2) {
        // If sum is odd, it's not possible
        return false;
    }

    for (auto & i: v) {
        sum_right -= i;
        sum_left += i;

        if (sum_left == sum_right)
            return true;

        if (sum_left > sum_right)
            return false;
    }

    return false;
}


int main() {
    std::vector<int> v { 7, 2, 4, 6, 0, 9, 3, 4, 0, 2, 1 };
    assert(CheckSplitInEqualSum(v) == true);

    std::vector<int> v1 { 0, 1 };
    assert(CheckSplitInEqualSum(v1) == false);
}